#! /bin/bash



cargo build > /dev/null;

for i in {1..20}
    do
    for algorithm in RabinKarp
    do
        for pcap in $(find /mnt/datapool/kieran/packets -name '*.cap' -or -name '*.pcap');
        do
            echo "cargo run $algorithm $pcap >> "/mnt/datapool/kieran/results/$algorithm-$i-`echo $pcap | rev | cut -d/ -f1 | rev`";"
            cargo run $algorithm $pcap >> "/mnt/datapool/kieran/results/$algorithm-$i-`echo $pcap | rev | cut -d/ -f1 | rev`";
        done
    done
done
