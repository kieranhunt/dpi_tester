extern crate dpi_algorithms;
extern crate pcap;
extern crate time;

use dpi_algorithms::ruleset::ruleset::Ruleset;
use dpi_algorithms::dpi_interface::algorithm::Algorithm;
use dpi_algorithms::dpi_interface::ahocorasick::AhoCorasick;
use dpi_algorithms::dpi_interface::bitap::Bitap;
use dpi_algorithms::dpi_interface::bloomfilter::BloomFilter;
use dpi_algorithms::dpi_interface::boyermoore::BoyerMoore;
use dpi_algorithms::dpi_interface::cuckoofilter::Cuckoo;
use dpi_algorithms::dpi_interface::horspool::Horspool;
use dpi_algorithms::dpi_interface::knuthmorrispratt::KnuthMorrisPratt;
use dpi_algorithms::dpi_interface::naive::Naive;
use dpi_algorithms::dpi_interface::rabinkarp::RabinKarp;
use dpi_algorithms::ruleset::ruleset::Algorithms;
use std::path::Path;
use std::env::args;
use std::error::Error;
use std::env;
use pcap::Capture;
use time::{Duration, PreciseTime};


fn main() {
    //TODO: Make this redirectable from stdin.

    let args: Vec<_> = env::args().collect();

    if args.len() != 3 {
        panic!("Incorrect number of arguments: {}", args.len());
    }

    let path = Path::new(&args[2]);

    let display = path.display();
    let mut cap = match Capture::from_file(path) {
        Err(why) => panic!("couldn't open {}: {}", display, Error::description(&why)),
        Ok(cap) => cap,
    };

    let ruleset = Ruleset::new(Algorithms::Naive, "amazonaws.com".to_string());

    let mut algorithm_ahocorasick = AhoCorasick::new(&ruleset);
    let mut algorithm_bitap = Bitap::new(&ruleset);
    let mut algorithm_bloom = BloomFilter::new(&ruleset);
    let mut algorithm_boyermoore = BoyerMoore::new(&ruleset);
    let mut algorithm_cuckoo = Cuckoo::new(&ruleset);
    let mut algorithm_horspool = Horspool::new(&ruleset);
    let mut algorithm_knuthmorrispratt = KnuthMorrisPratt::new(&ruleset);
    let mut algorithm_naive = Naive::new(&ruleset);
    let mut algorithm_rabinkarp = RabinKarp::new(&ruleset);

    let mut packet_count = 0;
    loop {
        let packet = match cap.next() {
            Some(x) => x,
            None => break,
        };

        let sub_start = PreciseTime::now();
        match args[1].to_string().as_ref() {
            "AhoCorasick" => {algorithm_ahocorasick.inspect_data(packet);},
            "Bitap" => {algorithm_bitap.inspect_data(packet);},
            "Bloom" => {algorithm_bloom.inspect_data(packet);},
            "BoyerMoore" => {algorithm_boyermoore.inspect_data(packet);},
            "Cuckoo" => {algorithm_cuckoo.inspect_data(packet);},
            "Horspool" => {algorithm_horspool.inspect_data(packet);},
            "KnuthMorrisPratt" => {algorithm_knuthmorrispratt.inspect_data(packet);},
            "Naive" => {algorithm_naive.inspect_data(packet);},
            "RabinKarp" => {algorithm_rabinkarp.inspect_data(packet);},
            _ => {panic!("Algorithm not implemented!")},
        }
        let sub_stop = PreciseTime::now();

        let elapsed_time_micro = sub_start.to(sub_stop).num_microseconds().unwrap();

        println!("{},{}", packet_count, elapsed_time_micro);
        packet_count = packet_count + 1;

    }
}
