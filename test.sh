#! /bin/bash

cargo build > /dev/null;

for algorithm in Bitap BoyerMoore Horspool KnuthMorrisPratt Naive RabinKarp
do
    for pcap in PCAPs/*
    do
        echo "cargo run $algorithm $pcap >> "results/$algorithm-`echo $pcap | rev | cut -d/ -f1 | rev`""
        cargo run $algorithm $pcap >> "results/$algorithm-`echo $pcap | rev | cut -d/ -f1 | rev`"
    done
done
